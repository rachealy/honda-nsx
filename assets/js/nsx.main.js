//honda.pages.nsxprereg.js
window.Honda = window.Honda || {};
window.Honda.pages = window.Honda.pages || {}
window.Honda.pages.nsxprereg = window.Honda.pages.nsxprereg || {};

(function ($, _, validation, ns, window, undefined) {

	ns.Init = function() {

        $("#nsx-hero, #nsx-history").YTPlayer();

        $('#txtPhone').inputmask('(99) 9999 9999');

        ns.BindUIEvents();
    
        //Flexslider
        $(window).load(function() {
            $('.nsx-features-flexslider').flexslider({
                selector: ".nsx-features-slides > li",
                slideshow: false,
                controlNav: false,
                animation: "fade",
                keyboard: false,
                directionNav: false,
                animationLoop: false,
                animationSpeed: 400,	    	
            });
    
            $('.nsx-details-flexslider').flexslider({
                selector: ".nsx-details-slides > li",
                slideshow: false,
                animation: "fade",
                controlNav: false,
                keyboard: false,
                directionNav: false,
                animationLoop: false,
                animationSpeed: 400,
            });
    
            $('.nsx-history-flexslider').flexslider({
                selector: ".nsx-history-slides > li",
                slideshow: false,
                animation: "fade",
                controlNav: false,
                keyboard: false,
                directionNav: false,
                animationLoop: false,
                animationSpeed: 400,
            });
        });
    };


	ns.BindUIEvents = function () {

        $("#btnRegister").click(function (evt) {
            evt.preventDefault();
            ns.ValidateForm();
        });

        //Play Video on Click
        $('.nsx-btn-play').on('click', function() {
            $('#nsx-hero').playYTP();
        });

        $('.nsx-btn-pause').on('click', function() {
            $('#nsx-hero').stopYTP();
            $('.nsx-btn-pause, .nsx-mute-btn').fadeOut(300);
            $('.nsx-btn-play, .nsx-logo, .nsx-hero-poster').fadeIn(300);
        });

        $('.nsx-mute-btn').on('click', function() {
            if($(this).hasClass('nsx-sound-on')) {
                $('#nsx-hero').muteYTPVolume();
                $(this).html('Sound <b>Off</b>');
                $(this).removeClass('nsx-sound-on').addClass('nsx-sound-off');
            } else {
                $('#nsx-hero').unmuteYTPVolume();
                $(this).html('Sound <b>On</b>');
                $(this).removeClass('nsx-sound-off').addClass('nsx-sound-on');
            }
        });


        $('.nsx-pause-toggle').on('click', function() {
            if($(this).hasClass('nsx-playing')) {
                $('#nsx-history').pauseYTP();
                $(this).html('Play');
                $(this).removeClass('nsx-playing').addClass('nsx-paused');
            } else {
                $('#nsx-history').playYTP();
                $(this).html('Pause');
                $(this).removeClass('nsx-paused').addClass('nsx-playing');
            }
        });
    

        $('#nsx-hero').on("YTPStart",function(e){
            setTimeout(function() {
                $('.nsx-btn-pause, .nsx-mute-btn').fadeIn(300);
                $('.nsx-btn-play, .nsx-logo, .nsx-hero-poster').fadeOut(300);
            }, 500);
        });

        $('.btn-hide-hotspots').on('click', function() {
            $('.nsx-tooltip').toggleClass('hidden');
            $(this).toggleClass('hidden');
        });

    	$('.nsx-features-nav li').on('click', function() {
            var navIndex = $(this).index();
            $('.nsx-features-nav li').removeClass('active');
            $(this).addClass('active');
            $('.nsx-features-flexslider').flexslider(navIndex);
        });
    
        $('.nsx-history-nav li').on('click', function() {
            var navIndex = $(this).index();
            $('.nsx-history-nav li').removeClass('active');
            $(this).addClass('active');
            $('.nsx-history-flexslider').flexslider(navIndex);
        });
    
        $('.nsx-tooltip').on('click', function() {
            var navIndex = $(this).data('slide');
            $('.nsx-details-flexslider').flexslider(navIndex);
    
            var detailsPanel = $('.nsx-panel-details');
    
            if (!detailsPanel.hasClass('open')) {
                detailsPanel.addClass('open');
            }
             
            $('html,body').animate({
                scrollTop: $("#nsx-details").offset().top
            }, 500);    
        });


        function numbersOnly(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if ( key < 48 || key > 57 ) {
                return false;
            }
            else return true;
        };

        $('#txtPostcode').on('keypress', function() {
            return numbersOnly(event);
        });
        
        //Smooth Scrolling
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 500);
                return false;
              }
            }
        });
	},

    ns.ValidateForm = function () {
            var valid = true;

            if(!validation.ValidateSelect("#selectTitle", "Please select your title")) { valid = false;}
            if(!validation.ValidateTextFields("#txtFirstName", "Please enter your first name")) { valid = false;}
            if(!validation.ValidateTextFields("#txtLastName", "Please enter your last name")) { valid = false;}
            if(!validation.ValidateEmail("#txtEmail", true)) { valid = false;}
            if(!validation.ValidatePhone("#txtPhone", true)) { valid = false;}
            if(!validation.ValidatePostcode("#txtPostcode","Please enter a 4 digit postcode")) { valid = false;}
            if(!validation.ValidateCheckbox("#chkTerms", "Please indicate that you have read and agree to Honda's privacy message")) { valid = false;}

            if(valid) {
               ns.PostForm("#btnRegister");
            } else {
                $('body, html').animate({scrollTop:$('#nsx-form').offset().top}, 'slow');
                console.log("can't post the form");
            }
        },

    ns.BindObject = function () {                                                                                    
        var obj = {
            "titleid": $('#selectTitle').val(), //["Dr", "Miss", "Mr", "Mrs", "Ms", "Other"]
            "firstname": $('#txtFirstName').val(),
            "surname": $('#txtLastName').val(),
            "email": $('#txtEmail').val(),
            "phoneNumber": $('#txtPhone').val(),
            "phonetestdrive": "",
            "address1": "",
            "address2": "",
            "suburb": "",
            "stateid": "", //["states:act", "states:nsw","states:nt", "states:qld", "states:sa", "states:tas", "states:vic", "states:wa"]
            "postcode1": $('#txtPostcode').val(),
            "postcode2": "",
            "postcodedealersearch": "",
            "inMarketDate": "",
            "models": ["NSX2015"],
            "vehicleModel": "NSX2015",
            "stayupdated": 0,
            "entrySource" : "",
            "potentialBuyer" : true,
            "SendmeNSXNews": false,
            "orderprintedbrochure": 0,
            "RequestTestDrive": 0,
            "ContactMe": false,
            "Categories":"honda-form:tagauchi/pre-registration",
            "ContactType":"honda-form:form-type/pre-registration",
            "preRegistrationName" : "NSX_2015",
            "utmCampaign":$("#utm_campaign").val(),
            "utmSource":$("#utm_source").val(),
            "utmMedium": "NSX 2015 Pre Registration",
            "utmTerm":$("#utm_term").val(),
            "utmContent":$("#utm_date").val(),
            "utmCreationDate":""
        };
         return obj;
    },

    ns.PostForm = function (elem) {
        Honda.log.log("Honda.pages.nsxprereg.PostForm");
        //var form = $("#form1");
        var honda_obj = ns.BindObject();

        var newLead = new Honda.components.LeadDetails({
            modelsInterested: honda_obj.models,
            "modelsInterested@TypeHint": "String[]",
            potentialBuyer: honda_obj.potentialBuyer,

        });

        var newContact = new Honda.components.ContactDetails({
            title: honda_obj.titleid,
            firstName: honda_obj.firstname,
            surname: honda_obj.surname,
            phone: honda_obj.phoneNumber,
            email: honda_obj.email
        });

        var newAddress = new Honda.components.AddressDetails({
            line1: honda_obj.address1,
            line2: honda_obj.Adaddress2dress2,
            suburb: honda_obj.suburb,
            state: honda_obj.stateid,
            postcode: honda_obj.postcode1
        });

        var newEnquiry = new Honda.components.EnquiryDetails({

        });

        var newSubscribe = new Honda.components.SubscribeDetails({
            stayUpdated: honda_obj.SendmeNSXNews,
            categories: honda_obj.Categories,
            contactMe: honda_obj.ContactMe
        });

        var newVehicle = new Honda.components.VehicleDetails({
            vehicleModel: honda_obj.models,
            "vehicleModel@TypeHint": "String[]",
        });

        var newDealer = new Honda.components.DealerDetails({

        });

        var newUTM = new Honda.components.UTMDetails({
            campaign: honda_obj.utmCampaign,
            source: honda_obj.utmSource,
            medium: honda_obj.utmMedium,
            term: honda_obj.utmTerm,
            content: honda_obj.utmContent,
            createdDate: honda_obj.utmCreationDate
        });

        var newContactType = new Honda.components.ContactTypeDetails({
            preRegistrationName: honda_obj.preRegistrationName,
            contactType: honda_obj.ContactType,
            "contactType@TypeHint": "String[]"
        });
        var isSuccess = Honda.content.createPage(Honda.services.endpoints.LEADS,
            [newLead, newContact, newAddress, newEnquiry, newSubscribe, newVehicle, newDealer, newUTM, newContactType], "lead", "honda.forms.nsxprereg");

        if (isSuccess)
            ns.PostFormSucessCB();
        else {
            if (!$(elem).next().hasClass('invalid')) {
                $(elem).after("<p class='invalid'>Error submitting data. Please try again later.</p>");
            }
        }


    },

    ns.PostFormSucessCB = function () {
		//window.location.href = "thank-you.html";
        $('.form-content').hide();
        $('.form-thanks').show();

    }

})(jQuery, _, Honda.forms.validation, Honda.pages.nsxprereg, this);
